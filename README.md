In order to make the Message Pack work in our scenario, I had to change the code.

This code was changed from the version 1.6.2 found at: https://github.com/neuecc/MessagePack-CSharp/releases/tag/v1.6.2 (source code)

To open this project, you'll probably need to install all the MS crap, but before installing 8GB of trash, try to open the solution and see if it imports most of the projects (or at least MessagePack.CodeGenerator). If it can open the projects, you'll probably need to install everything.

Install:

	- https://www.visualstudio.com/downloads/ (Build Tools for Visual Studio 2017)
		- Select all options (C++, Web, Core)
		- Select every single .NET Framework version
		
After installing (and reestarting the computer), you should be able to open the solutions with all project (except MessagePackAnalyser.Vsix). If not, I don't know what to do there, maybe upgrading Visual Studio or try to open using the Jetbrain's Rider IDE.

Once the project is open, compile it. It will most likely compile without problems.

Set the project MessagePack.CodeGenerator as the startup project; then go to the project's properties, then Debug, then type the command line args you want to pass into the program, an example would be: 

-i "..\TO\TO.csproj" -o "..\TO\MessagePack\TOMessagePackGenerated.cs" -n TO.MessagePackGenerate

Note that the paths are relative, so you'll need to define the working directory as the "mpc" folder that is inside our backend project - or just use absolute paths.

Now you can run. It should run without problems, but sometimes some NuGet packages just don't get restored or are broken for some reason.

Go to "Manage NuGet Packages" (of the project). The packages that matters to us are: Microsoft.Composition and Microsoft.CodeAnalysis (and all dependencies/related/variants of these packages). It is currenctly working using the versions:

Microsoft.Composition 1.0.31
Microsoft.CodeAnalysis.* 2.6.1
Microsoft.CodeAnalysis.Elfie 0.10.6-rc2
Microsoft.CodeAnalysis.Analyzers 2.6.0

You can try to upgrade/downgrade these versions if it's not working for some reason.

Go this project folder, delete the "packages" directory. Then go to the NuGet package manager and restore the packages.

Everything should be working now.

I basically just changed code in RoslynExtensions and the templated at "Generator" folder (to add global:: before "using System", making "using global::System" to prevent errors on the exported file).

Explanation:


Code generator:

	The message pack protocol precompiles all serializers into a class. This is done to prevent having to generate this serializers in runtime, which AOT (Unity iOS and Android) compilation doesn't allow.
	
	In order to generate this serializers, it need to parse all of our classes, so it uses the Roslyn (the .NET Compiler toolchain) to do that.
	
	When we pass our project path, Roslyn loads the csproj - just like Visual Studio does. Roslyn also includes all DLL and other csproj referenced by the project. This enables us to generate compilations and reason about the code itself, including getting metadata about properties, classes, attribues, etc (which MessagePack uses to identify which properties have [Key] or which classes have [MessagePackObject] in order to generate serializers for them).
	
	The problem is that, for some reason, when a project references another project, Roslyn doesn't load the DLLs reference by the project. 
	
	So, for example, if I had two projects: TestA and TestB.
	
	TestA references System.dll.
	TestB references System.dll and TestA.
	
	If we pass TestA to Roslyn, it will load the TestA project with the System.dll as reference - as you'd expect.
	If we pass TestB to Roslyn, it will load the TestB project with TestA as reference, but not with System.dll referenced.
	
	The problem with this is that every class on TestB that happens to reference anything on System.dll will result in error.
	
	The message pack code already accounted for this. It used a hack:
	
		typeof(System.Object).Assembly.Location
		typeof(Enumerable).Assembly.Location
		
	This will give us the location of both mscorlib.dll and System.Core. MessagePack then added these paths as references in our project.
	
	The problem is that it only did that on the main project. Not on projects referenced by the main project. So the main project loaded correctly, but every problem referenced by it that had another project as reference, would suffer from the same problem.
	
Solution:

	What I did was to create a special type of workspace (a temporary one) that gives us control to add documents, projects, etc.
	
	I load our project using the normal namespace, duplicate all projects and documents, add the necessary reference to all projects (not only the main project) and use this new workspace to generate the compilation and reason about the code. Which means that every single project on the workspace have the necessary references, so everything works fine.
	
	I couldn't use the normal workspace to do this because it required that we apply our changes, which writes what we changed to our project files - which we don't wanna do.